#lang racket

(require "ide-frame.rkt")
(require "ide-editor.rkt")
(require "ide-menu.rkt")

(define ide%
  (class object%
    
    (define ide-frame (new ide-frame%
                           [label "plumbing ide"]))

    (define ide-text  (new ide-editor%
                           [parent ide-frame]))

    (define ide-menu (new ide-menu%
                          [parent ide-frame]))
    
    (send ide-frame show #true)
    
    (super-new)
    ))

(new ide%)
