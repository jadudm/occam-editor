#lang racket/gui

(provide ide-editor%)


(define ide-text%
  (class text%

    (super-new)
    ))

(define ide-editor%
  (class editor-canvas%
    (define the-text (new ide-text%))

    (send this set-editor the-text)
    
    (super-new)))
